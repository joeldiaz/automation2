#donde se encuentra el archivo
#/home/joel/Docker/Automation/scan.xml
import xml.dom.minidom

def LeeScanXml():

#leemos el archivo creado por nmap y parseamos XML
   doc = xml.dom.minidom.parse("scan.xml")


#de la lista del archivo XML tomamos el tag "state" puede ser open o close
   state = doc.getElementsByTagName("state")
   aviso = "El puerto se encuentra: "
   for skill in state :
     return (aviso +  skill.getAttribute("state"))

#if __name__ == "__main__":
#    LeeXML()

def LeeScanXml_rdp():

#leemos el archivo creado por nmap y parseamos XML
   doc = xml.dom.minidom.parse("scan_rdp.xml")


#de la lista del archivo XML tomamos el tag "state" puede ser open o close
   state = doc.getElementsByTagName("state")
   aviso = "El acceso remoto se encuentra: "
   for skill in state :
     return (aviso +  skill.getAttribute("state"))
