FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN python3 -m pip install -r requirements.txt
RUN apt-get update && apt-get install -y nmap netcat telnet gawk
COPY . /code/
