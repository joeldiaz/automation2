from django_tables2.utils import A
from django_tables2.config import RequestConfig
from django_tables2.export.export import TableExport
from django_tables2 import tables, TemplateColumn, LinkColumn, Column, Table
from AutomationApp.models import ModelName
TEMPLATE= """
        
		  <a class="add" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
          <a class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
          <a class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>

"""




class InventarioTable(tables.Table):
    actions = TemplateColumn(template_name="bootstrap/columna.html")
    class Meta:
        model = ModelName
        template_name = "django_tables2/bootstrap4.html"
        fields = ("ip","mascara","nombre","detalle","actions")
        #<th>IP</th>
	    #ip = models.GenericIPAddressField(max_length=200, null=True)
	    #<th>Mascara</th>
	    #mascara = models.GenericIPAddressField(max_length=200, null=True)
	    #<th>Nombre</th>
	    #nombre = models.CharField(max_length=200, null=True)
	    #<th>Detalle</th>
	    #detalle = models.CharField(max_length=200, null=True)


class ModelNameTable(Table):
    status = Column(accessor='status', verbose_name='Status')
    #mascara = Column(accessor='mascara', verbose_name='Mascara')
    #nombre = Column(accessor='nombre', verbose_name='Nombre')
    #detalle = Column(accessor='detalle', verbose_name='Detalle')

    class Meta:
        model = ModelName