from django.db import models

# Create your models here.
#el modelo es la tabla en la base de datos
class Inventarios(models.Model):
	#<th>IP</th>
	ip = models.GenericIPAddressField(max_length=200, null=True)
	#<th>Mascara</th>
	mascara = models.GenericIPAddressField(max_length=200, null=True)
	#<th>Nombre</th>
	nombre = models.CharField(max_length=200, null=True)
	#<th>Detalle</th>
	detalle = models.CharField(max_length=200, null=True)

	def __str__(self):
		return self.nombre

class ContactoSuc(models.Model):
#<th>Region</th>
    region = models.CharField(max_length=200, null=True)
#<th>Sucursal</th>
    sucursal = models.CharField(max_length=200, null=True)
#<th>Telefono Suc</th>
    telefonoSuc = models.CharField(max_length=200, null=True)
#<th>Direccion</th>
    direccion = models.CharField(max_length=200, null=True)
#<th>Telefonos</th>
    telefonos = models.CharField(max_length=200, null=True)
#<th>Horario de Atencion</th>
    horarioAtencion = models.CharField(max_length=200, null=True)
#<th>Referente</th>
    referente = models.CharField(max_length=200, null=True)
#<th>Celular</th>
    celular = models.CharField(max_length=200, null=True)
    def __str__(self):
	    return  self.sucursal

class Usuarios(models.Model):
	nombre = models.CharField(max_length=200, null=True)
	apellido = models.CharField(max_length= 200, null=True)
	email = models.CharField(max_length=200, null=True)
	fechaAlta = models.DateTimeField(auto_now_add=True)
	password1 = models.CharField(max_length=200, null=True)
	password2 = models.CharField(max_length=200, null=True)

	#la funcion __str__ permite ver el valor de la variable en
	#el dashboard admin
	def __str__(self):
		return self.nombre


class ModelName(models.Model):
	status = models.CharField(max_length=200, null=True)

