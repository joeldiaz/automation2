import django_filters
from .forms import *
from AutomationApp.models import Inventarios
class InventarioFilter(django_filters.FilterSet):
    class Meta:
        form = Consultas
        fields = ['ip','mascara','nombre','detalle']



class ModelNameFilter(django_filters.FilterSet):
    class Meta:
        model = ModelName
        fields = ['status']
