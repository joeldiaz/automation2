import subprocess
from pyexpat.errors import messages

from django.views.generic.base import TemplateView
from django.forms import inlineformset_factory
from django_tables2.config import RequestConfig
from django_tables2.export.export import TableExport
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin

from .decorators import unauthenticated_user
from .tables import ModelNameTable
from django_filters.views import FilterView
from .forms import *


from django.http import HttpResponse, request
from netmiko import ConnectHandler

from .forms import *
from django.forms import inlineformset_factory
from AutomationApp.models import *
from django.shortcuts import render, redirect
import os
import sys
from subprocess import Popen, PIPE, check_output
import socket
from .comandos import *
from .filters import *
# importamos los metodos que automatizan la elección de interface en FTD
# y ejecución de netmiko
from django.urls import reverse_lazy
import nmap
import re
#modulo para leer xml
import xml.dom.minidom
from xmlreader import LeeScanXml, LeeScanXml_rdp
from llamaxmlreader import ejecutaLeeScanXml, ejecutaLeeScanXml_rdp
#se importan las tablas
from django_tables2 import SingleTableView
from .tables import InventarioTable
from .models import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .decorators import unauthenticated_user ,allowed_users, admin_only
from django.contrib.auth.models import Group
from django.contrib import messages




# Create your views here.

# pagina de loging de user

def login_page(request):
		if request.method == 'POST':
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('home')
			else:
				messages.info(request, 'Usuario o  password incorrectos ')

		context = {}
		return render(request, 'bootstrap/login.html', context)

def logoutUser(request):
	logout(request)
	return redirect('login')


#pagina de registro de usuario
def register_page(request):
    form = UsuariosForm()
    if request.method == 'POST':
        form = UsuariosForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')

            group = Group.objects.get(name='guest')
            user.groups.add(group)

            messages.success(request, 'La cuenta para ' + username + 'se creó correctamente')

            return redirect('login')

    context = {'form': form}
    return render(request, 'bootstrap/register.html', context)
# pagina principal


# metodo que retorna el view en al url

#@login_required()

def home_page(request):

    if view_server(request):
        return view_server(request)
    if view_pagina(request):
        return view_pagina(request)
    if view_RDP(request):
        return view_RDP(request)
    #if TomoIP(request):
    #    return TomoIP(request)
    if Port_view(request):
        return Port_view(request)
    return render(request, 'bootstrap/index.html')



def test_page(request):
    if Port_view(request):
        return Port_view(request)
#inventario
"""
def inventario_page(request):
    if request.method == 'POST':
        form_server = Inventario(request.POST)
        if form_server.is_valid():
            ip = request.POST.get('ip', '')
            mascara = request.POST.get('mascara', '')
            nombre = request.POST.get('nombre', '')
            detalle = request.POST.get('detalle','')
            context_server = {
                'form_server': form_server,

            }
    return render(request, 'bootstrap/inventario.html')

"""
"""
#crear inventario sucursales
def crear_inventario(request, pk):
    InventarioSucFormSet = inlineformset_factory(Usuarios, ContactoSuc, fields=('region','sucursal'))
    usuario = Usuarios.objects.get(id=pk)
    formset = InventarioSucFormSet(queryset = ContactoSuc.onjects.none(), instance = usuario)
    if request.method == 'POST':
        formset = InventarioSucFormSet( request.POST, instance=usuario)
        if formset.is_valid():
            formset.save()
            return redirect('/')
    context = {
        "form": formset,
    }
    return render(request, 'bootstrap/contactossuc.html', context)


"""

#test para el view de formularios crispy y table2



class FilteredSingleTableView(SingleTableMixin, FilterView):
    formhelper_class = None


    def get_filterset(self, filterset_class):
        kwargs = self.get_filterset_kwargs(filterset_class)
        filterset = filterset_class(**kwargs)
        filterset.form.helper = self.formhelper_class()
        return filterset


class ModelNameView(FilteredSingleTableView):
    template_name = 'inventario.html'
    table_class = ModelNameTable
    paginate_by = 25
    filterset_class = ModelNameFilter
    formhelper_class = ModelNameFilterFormHelper

"""
class inventario_table(SingleTableView, FilterView):
    model = Inventarios
    table_class = InventarioTable
    queryset = Inventarios.objects.all()
    template_name = 'bootstrap/inventario.html'
    def get_filterset(self, filterset_class):
        kwargs = self.get_filterset_kwargs(filterset_class)
        filterset = filterset_class(**kwargs)
        filterset.form.helper = self.formhelper_class()
        return filterset
    def inventario_page(self, request, *args, **kwargs):
        table = InventarioTable(Inventarios.objects.all())
        RequestConfig(request).configure(table)
        export_format = request.GET.get("_export", None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, table)
            return exporter.response("table.{}".format(export_format))

        return render(request, 'bootstrap/inventario.html', {"table": table})



class ModelNameView(inventario_table):
    template_name = 'modelname.html'
    table_class = InventarioTable
    paginate_by = 25
    filterset_class = InventarioFilter
    formhelper_class = Inventario

"""


#usuario

def usuario_page(request, pk_test):
    usuario = Usuarios.objects.get(id=pk_test)
    inventario = usuario.order_set.all()
    context = {
        "usuario" : usuario,
        "inventario": inventario,

    }
    return render(request, 'bootstrap/usuario.html', context)


#inventario contacto sucursales
def invenContactossuc_page(request):
    contactoSuc = ContactoSuc.objects.all()
    context = {
        "contactoSuc" : contactoSuc,
    }
    return render(request, 'bootstrap/contactossuc.html', context)


#configurar botón agregar campos de formulario para agregar datos a base
#y mostrarlos en el form

"""
#crear inventario sucursales
def crear_inventario(request, pk):
    InventarioSucFormSet = inlineformset_factory(Usuarios, ContactoSuc, fields=('region','sucursal'))
    usuario = Usuarios.objects.get(id=pk)
    formset = InventarioSucFormSet(queryset = ContactoSuc.onjects.none(), instance = usuario)
    if request.method == 'POST':
        formset = InventarioSucFormSet( request.POST, instance=usuario)
        if formset.is_valid():
            formset.save()
            return redirect('/')
    context = {
        "form": formset,
    }
    return render(request, 'bootstrap/contactossuc.html', context)


"""
#inventario equipos
def invenEquipos_page(request):
    return render(request, 'bootstrap/equipos.html')
#topologia ibm
def topoIbm_page(request):
    return render(request, 'bootstrap/ibm.html')

#topologia ibera
def topoIbera_page(request):
    return render(request, 'bootstrap/ibera.html')
#topologia sucursales
def topoSucursales_page(request):
    return render(request, 'bootstrap/sucursales.html')

#seccion graficos

def dcibera_page(request):
    return render(request, 'bootstrap/dcibera.html')

def estadoRed_page(request):
    return render(request, 'bootstrap/estadoRed.html')
def estadoWeb_page(request):
    return render(request, 'bootstrap/estadoWeb.html')
def detalleServicioIBM_page(request):
    return render(request, 'bootstrap/detalleServicioIBM.html')
def detalleServicioIBERA_page(request):
    return render(request, 'bootstrap/detalleServicioIBERA.html')
def detalleServicioVDI_page(request):
    return render(request, 'bootstrap/detalleServicioVDI.html')
def holding_page(request):
    return render(request, 'bootstrap/holding.html')
def cac_page(request):
    return render(request, 'bootstrap/cac.html')

# metodo que devuelve el contexto de los campos de SERVER
def view_server(request):
    if request.method == 'POST':
        form_server = Consultas(request.POST)
        if form_server.is_valid():
            origen = request.POST.get('origen', '')
            destino = request.POST.get('destino', '')
            output_ping = check_server(origen, destino)
            context_server = {
                'form_server': form_server,
                'output': output_ping,
            }
            return render(request, 'bootstrap/index.html', context_server)
    else:
        form_server = Consultas()
        return render(request, 'bootstrap/index.html', {'form': form_server})


# metodo que devuelve el contexto de los campos de URL
def view_pagina(request):
    if request.method == 'POST':
        form_pagina = Consultas_URL(request.POST)
        if form_pagina.is_valid():
            url = request.POST.get('url', '')
            port_url = request.POST.get('port_url', '')
            output_pagina = check_pagina(url, port_url)
            context_pagina = {
                'form_pagina': form_pagina,
                'output': output_pagina,
            }
            return render(request, 'bootstrap/index.html', context_pagina)
    else:
        form_pagina = Consultas_URL()
        return render(request, 'bootstrap/index.html', {'form': form_pagina})


# metodo que devuelve el contexto de los campos RDP
def view_RDP(request):
    if request.method == 'POST':
        form_rdp = Consultas_RDP(request.POST)
        if form_rdp.is_valid():
            destino_rdp = request.POST.get('destino_rdp', '')
            output_rdp = check_RDP(destino_rdp)
            context_rdp = {
                'form_rdp': form_rdp,
                'output': output_rdp,
            }
            return render(request, 'bootstrap/index.html', context_rdp)
    else:
        form_rdp = Consultas_RDP()
        return render(request, 'bootstrap/index.html', {'form': form_rdp})




# acá van los metodos que ejecutan los comandos
# comando ping origen destino
def check_server(origen, destino):
    ping = subprocess.check_output(["ping", "-c", "5", "-S", origen, destino])

    output = subprocess.getoutput(str(' %s \n' % ping))
    return output


# comando ping a url
def check_pagina(url, port_url):
    #ok = "Pagina ok"
    #notok = "Pagina not ok"
    #telnet = subprocess.check_output(["nc", "-w1", "-v", url, port_url, "2>&1", "|", " awk", '{print }'])
    #print(telnet)
    #nc - 4 - w3 - vv www.experta.com.ar 44 > & 1 | awk '{ if(/succeeded/) print "La pagina está ok"; else print "No se puede alcanzar la pagina" }'
    comando = subprocess.check_output(["nmap","-oX","scan.xml", "-p", port_url, url])

    return ejecutaLeeScanXml()

# nmap -p 3389 172.19.46.7
# subprocess.check_output(["nmap", "-p", "3389", "172.23.2.18"])
def check_RDP(destino_rdp):
    comando = subprocess.check_output(["nmap","-oX","scan_rdp.xml", "-p", "3389", destino_rdp])
    # comando = subprocess.check_output(["nmap","-A   ", destino_rdp])
    #output = subprocess.getoutput(str(' %s \n' % comando))
    #return output
    return ejecutaLeeScanXml_rdp()

#interfaces del FW
DMZ_Servers_20 = "DMZ_Servers_20"
DMZ_CISCO = "DMZ_CISCO"
DMZ_21 = "DMZ_21"
DMZ_Camaras = "DMZ_Camaras"

#defino las variables con las ip de los Fw de arcos e ibera
equipoIbera = "172.21.222.1"
equipoArcos = "172.19.43.100"


def Port_view(request):
    # llamo al metodo POST
    if request.method == 'POST':
        # inicializo el formulario
        form_port = Consultas_Port(request.POST)
        # si el formulario es valido
        if form_port.is_valid():
           # tomo los valores del frmulario
            udp_tcp = request.POST.get('udp_tcp', '')
            iporigen_port = request.POST.get('iporigen_port', '')
            portori_port = request.POST.get('portori_port', '')
            ipdestino_port = request.POST.get('ipdestino_port', '')
            portdest_port = request.POST.get('portdest_port', '')
            # declaro las variables de los equipos segun rango de red
            InsideIbera = re.findall("^172.21.222", iporigen_port)
            LanIbera = re.findall("^172.21.1", iporigen_port)
            DMZIbera = re.findall("^192.168.21", iporigen_port)
            InsideArcos = re.findall("^172.19", iporigen_port)
            DMZServer = re.findall("^192.168.20", iporigen_port)
            DMZCISCO = re.findall("^192.168.30", iporigen_port)
            DMZ_CAM = re.findall("^192.168.22", iporigen_port)

            def ElijeFW():
                ip = "0"
                if DMZIbera:
                    ip = equipoIbera
                if DMZ_CAM:
                    ip = equipoIbera
                if DMZServer:
                    ip = equipoArcos
                if DMZCISCO:
                    ip = equipoArcos
                return ip

            def ElijeInterface():
                Interface = ""
                if DMZIbera:
                    Interface = DMZ_21
                if DMZServer:
                    Interface = DMZ_Servers_20
                if DMZCISCO:
                    Interface = DMZ_CISCO
                if DMZ_CAM:
                    Interface = DMZ_Camaras
                return Interface

            equipo = ElijeFW()
            Interface = ElijeInterface()
            def ejecuta(Interface, udp_tcp, iporigen_port, portori_port, ipdestino_port, portdest_port):
                if InsideIbera or InsideArcos or LanIbera:
                    output_ejecuta  = "El equipo no pasa por Firewall"
                else:
                    cisco1 = {
                        "device_type": "autodetect",
                        "host": equipo,
                        "username": "admin",
                        "password": "secundop150",
                    }
                    com=f"packet-tracer input {Interface} {udp_tcp} {iporigen_port} {portori_port} {ipdestino_port} {portdest_port} | grep Action"
                    #com = request.POST.get('udp_tcp', '')
                    print(com)
                    net_connect = ConnectHandler(**cisco1)

                    output_ejecuta = net_connect.send_command(com)
                return output_ejecuta

            output_port = ejecuta(Interface, udp_tcp, iporigen_port, portori_port, ipdestino_port, portdest_port)
            context_port = {
                'form_port': form_port,
                'output': output_port,
            }
            return render(request, 'bootstrap/index.html', context_port)

    else:
        form_port = Consultas_Port()
        return render(request, 'bootstrap/index.html', {'form': form_port})
