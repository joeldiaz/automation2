from django.contrib import admin

# Register your models here.

from .models import *
#registro el modelo de la base de datos de Inventario
admin.site.register(Inventarios)
#registro el modelo de la base de datos de Usuarios
admin.site.register(Usuarios)
#registro Contacto Sucursales
admin.site.register(ContactoSuc)
#registro Registro de usuarios

admin.site.register(ModelName)