import subprocess

from django.shortcuts import render
from django.http import HttpResponse, request
from netmiko import ConnectHandler

from .forms import *
from django.forms import inlineformset_factory
from .models import *
from django.shortcuts import render, redirect
import os
import sys
from subprocess import Popen, PIPE, check_output
import socket
from .comandos import *
from .filters import *
# importamos los metodos que automatizan la elección de interface en FTD
# y ejecución de netmiko
from django.urls import reverse_lazy
import nmap
import re


# Create your views here.

# pagina de loging de user
def login_page(request):
    return render(request, 'bootstrap/login.html')


# pagina principal

# metodo que retorna el view en al url
def home_page(request):
    if view_server(request):
        return view_server(request)
    if view_pagina(request):
        return view_pagina(request)
    if view_RDP(request):
        return view_RDP(request)
    #if TomoIP(request):
    #    return TomoIP(request)
    if Port_view(request):
        return Port_view(request)

def test_page(request):
    if Port_view(request):
        return Port_view(request)

# metodo que devuelve el contexto de los campos de SERVER
def view_server(request):
    if request.method == 'POST':
        form_server = Consultas(request.POST)
        if form_server.is_valid():
            origen = request.POST.get('origen', '')
            destino = request.POST.get('destino', '')
            output_ping = check_server(origen, destino)
            context_server = {
                'form_server': form_server,
                'output': output_ping,
            }
            return render(request, 'bootstrap/index.html', context_server)
    else:
        form_server = Consultas()
        return render(request, 'bootstrap/index.html', {'form': form_server})


# metodo que devuelve el contexto de los campos de URL
def view_pagina(request):
    if request.method == 'POST':
        form_pagina = Consultas_URL(request.POST)
        if form_pagina.is_valid():
            url = request.POST.get('url', '')
            output_pagina = check_pagina(url)
            context_pagina = {
                'form_pagina': form_pagina,
                'output': output_pagina,
            }
            return render(request, 'bootstrap/index.html', context_pagina)
    else:
        form_pagina = Consultas_URL()
        return render(request, 'bootstrap/index.html', {'form': form_pagina})


# metodo que devuelve el contexto de los campos RDP
def view_RDP(request):
    if request.method == 'POST':
        form_rdp = Consultas_RDP(request.POST)
        if form_rdp.is_valid():
            destino_rdp = request.POST.get('destino_rdp', '')
            output_rdp = check_RDP(destino_rdp)
            context_rdp = {
                'form_rdp': form_rdp,
                'output': output_rdp,
            }
            return render(request, 'bootstrap/index.html', context_rdp)
    else:
        form_rdp = Consultas_RDP()
        return render(request, 'bootstrap/index.html', {'form': form_rdp})

# acá van los metodos que ejecutan los comandos
# comando ping origen destino
def check_server(origen, destino):
    ping = subprocess.check_output(["ping", "-c", "5", "-S", origen, destino])
    output = subprocess.getoutput(str(' %s \n' % ping))
    return output


# comando ping a url
def check_pagina(url):
    ping = subprocess.check_output(["ping", "-c", "5", url])
    output = subprocess.getoutput(str(' %s \n' % ping))
    return output


# nmap -p 3389 172.19.46.7
# subprocess.check_output(["nmap", "-p", "3389", "172.23.2.18"])
def check_RDP(destino_rdp):
    comando = subprocess.check_output(["nmap", "-p", "3389", destino_rdp])
    # comando = subprocess.check_output(["nmap","-A   ", destino_rdp])
    output = subprocess.getoutput(str(' %s \n' % comando))
    return output

#interfaces del FW
DMZ_Servers_20 = "DMZ_Servers_20"
DMZ_CISCO = "DMZ_CISCO"
DMZ_21 = "DMZ_21"
DMZ_Camaras = "DMZ_Camaras"

#defino las variables con las ip de los Fw de arcos e ibera
equipoIbera = "172.21.222.1"
equipoArcos = "172.19.43.100"
"""
def NoPasaPorFW():
    if InsideIbera:
        print("El equipo no pasa por el FW")
    if InsideArcos:
        print("El equipo no pasa por el FW")
"""



def Port_view(request):
    # llamo al metodo POST
    if request.method == 'POST':
        # inicializo el formulario
        form_port = Consultas_Port(request.POST)
        # si el formulario es valido
        if form_port.is_valid():
            from netmiko import ConnectHandler
            # tomo los valores del frmulario
            udp_tcp = request.POST.get('udp_tcp', '')
            iporigen_port = request.POST.get('iporigen_port', '')
            portori_port = request.POST.get('portori_port', '')
            ipdestino_port = request.POST.get('ipdestino_port', '')
            portdest_port = request.POST.get('portdest_port', '')
            # declaro las variables de los equipos segun rango de red
            InsideIbera = re.findall("^172.21.222", iporigen_port)
            DMZIbera = re.findall("^192.168.21", iporigen_port)
            InsideArcos = re.findall("^172.19", iporigen_port)
            DMZServer = re.findall("^192.168.20", iporigen_port)
            DMZCISCO = re.findall("^192.168.30", iporigen_port)
            DMZ_CAM = re.findall("^192.168.22", iporigen_port)

            def ElijeFW():
                ip = "0"
                if DMZIbera:
                    ip = equipoIbera
                if DMZ_CAM:
                    ip = equipoIbera
                if DMZServer:
                    ip = equipoArcos
                if DMZCISCO:
                    ip = equipoArcos
                return ip

            def ElijeInterface():
                Interface = ""
                if DMZIbera:
                    Interface = DMZ_21
                if DMZServer:
                    Interface = DMZ_Servers_20
                if DMZCISCO:
                    Interface = DMZ_CISCO
                if DMZ_CAM:
                    Interface = DMZ_Camaras
                return Interface
            #inicializo las funciones ElijeFw() y ElijeInterface()
            equipo = ElijeFW()
            Interface = ElijeInterface()
            #Codigo de netmiko para conectar y ejecutar comando
            cisco1 = {
                "device_type": "autodetect",
                "host": equipo,
                "username": "admin",
                "password": "secundop150",
            }
            com=f"packet-tracer input {Interface} {udp_tcp} {iporigen_port} {portori_port} {ipdestino_port} {portdest_port}"
            #com = request.POST.get('udp_tcp', '')
            print(com)
            net_connect = ConnectHandler(**cisco1)

            output_port = net_connect.send_command(com)
            context_port = {
                'form_port': form_port,
                'output': output_port,
            }
            return render(request, 'bootstrap/index.html', context_port)

    else:
        form_port = Consultas_Port()
        return render(request, 'bootstrap/index.html', {'form': form_port})

