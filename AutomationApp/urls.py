#acá van las URLS de la APP
from django.contrib import admin
from django.urls import path, include
from AutomationApp.views import *
from . import views
from . import forms

urlpatterns = [
    path('login/', views.login_page, name="login"),
    path('register/', views.register_page, name="register"),
    path('home/', views.home_page, name="home"),
    #path('inventario/', inventario_table.as_view(), name = "inventario"),
    #path('inventario/', FilteredSingleTableView.as_view(), name = "inventario"),
    #path('inventario/', FilteredSingleTableView.as_view(filterset_class=ModelNameFilter,template_name='inventario'), name='searcher'),
    path('contactossuc/', views.invenContactossuc_page, name="contactossuc"),
    path('equipos/', views.invenEquipos_page, name="equipos"),
    path('ibm/', views.topoIbm_page, name="ibm"),
    path('ibera/', views.topoIbera_page, name="ibera"),
    path('sucursales/', views.topoSucursales_page, name="sucursales"),
    path('estadoRed/', views.estadoRed_page, name="estadoRed"),
    path('estadoWeb/', views.estadoWeb_page, name="estadoWeb"),
    path('detalleServicioIBM/', views.detalleServicioIBM_page, name="detalleServicioIBM"),
    path('detalleServicioIBERA/', views.detalleServicioIBERA_page, name="detalleServicioIBERA"),
    path('detalleServicioVDI/', views.detalleServicioVDI_page, name="detalleServicioVDI"),
    path('dcibera/', views.dcibera_page, name="dcibera"),
    path('cac/', views.cac_page, name="cac"),
    path('holding/', views.holding_page, name="holding"),
    path('test/', views.test_page, name="test"),
]


