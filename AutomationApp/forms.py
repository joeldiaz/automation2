#acá va el core de los formularios
from django import forms
#para poder interactuar con los modelos y los forms
#es necesario importar los siguientes modulos
from django.forms import ModelForm
from .models import *
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User





Consulta = [
    ('NoLlegoPagina', 'No llego a una página'),
    ('NoLlegoServer', 'No llego a un server'),
    ('NoRemoto', 'No tengo acceso remoto'),
    ('PtoCerrado', 'Parece que el puerto está cerrado'),
]
class dropdown(forms.Form):
    widget = forms.Select(choices=Consulta)

class Consultas(forms.Form):
    origen = forms.CharField()
    destino = forms.CharField()

class Consultas_URL(forms.Form):
    url = forms.CharField()
    port_url = forms.CharField()

class Consultas_Port(forms.Form):
    udp_tcp = forms.CharField
    iporigen_port = forms.CharField()
    portori_port = forms.CharField()
    ipdestino_port = forms.CharField()
    portdest_port = forms.CharField()


class Consultas_RDP(forms.Form):
    destino_rdp = forms.CharField()
class CmdForm(forms.Form):
    command = forms.CharField(label='Command to execute')

class GetName(forms.Form):
    myhostname = forms.CharField(label='Ingrese IP')

class Inventario(FormHelper):
    form_method = 'GET'
    layout = Layout(
        Fieldset(
            'ip',
            'mascara',
            'nombre',
            'detalle',
        ),

        Submit('submit', 'Apply Filter'),
    )

"""
    ip = forms.GenericIPAddressField()
    mascara = forms.GenericIPAddressField()
    nombre = forms.CharField()
    detalle = forms.CharField()
"""


class ContactoSucu(ModelForm):
	class Meta:
		sucursal = ContactoSuc
		fields = '__all__'



class ModelNameFilterFormHelper(FormHelper):
    form_method = 'GET'
    layout = Layout(
        'status',
    )


"""
class Usuarios(models.Model):
	nombre = models.CharField(max_length=200, null=True)
	apellido = models.CharField(max_length= 200, null=True)
	email = models.CharField(max_length=200, null=True)
	fechaAlta = models.DateTimeField(auto_now_ad    d=True)
	password1 = models.CharField(max_length=200, null=True)
	password2 = models.CharField(max_length=200, null=True)
"""
class UsuariosForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email','password1','password2']

