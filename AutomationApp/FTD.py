# !/usr/bin/env python
from netmiko import ConnectHandler
from getpass import getpass
import re
from .forms import *
from django.http import HttpResponse, request
from django.shortcuts import render


# defino las variables que representan los Firewalls
equipoIbera = "172.21.222.1"
equipoArcos = "172.19.43.100"
equipo = "0"
interface = "0"
"""
def TomoIP(request):
    #llamo al metodo POST
    if request.method == 'POST':
        #inicializo el formulario
        form_port = Consultas_Port(request.POST)
        #si el formulario es valido
        if form_port.is_valid():
            #tomo los valores del frmulario
            udp_tcp = request.POST.get('udp_tcp', '')
            iporigen_port = request.POST.get('iporigen_port', '')
            ipdestino_port = request.POST.get('ipdestino_port', '')
            portori_port = request.POST.get('portori', '')
            portdest_port = request.POST.get('portdest', '')
            #declaro las variables de los equipos segun rango de red
            InsideIbera = re.findall("^172.21.222", iporigen_port)
            DMZIbera = re.findall("^192.168.21", iporigen_port)
            InsideArcos = re.findall("^172.19", iporigen_port)
            DMZServer = re.findall("^192.168.20", iporigen_port)
            DMZCISCO = re.findall("^192.168.30", iporigen_port)
            DMZ_CAM = re.findall("^192.168.22", iporigen_port)
            #declaro las interfaces
            DMZ_Servers_20 = "DMZ_Servers_20"
            DMZ_CISCO = "DMZ_CISCO"
            DMZ_21 =  "DMZ_21"
            DMZ_Camaras = "DMZ_Camaras"
            interface = DMZ_CISCO
            #si esta dentro de la LAN no pasa por fw
            if InsideIbera:
                print("El equipo no pasa por el FW")
            if InsideArcos:
                print("El equipo no pasa por el FW")
            #de lo contrario elijo fw segun network
            if DMZIbera:
                equipo = equipoIbera
                #interface = DMZ_21
            if DMZServer:
                equipo = equipoArcos
                #interface = DMZ_Servers_20
            if DMZCISCO:
                equipo = equipoArcos
                #interface = DMZ_CISCO
            if DMZ_CAM:
                equipo = equipoIbera
                #interface = DMZ_Camaras
            output_port = deviceConnection(interface,equipo, udp_tcp, iporigen_port, ipdestino_port, portori_port, portdest_port)
            context_port = {
                'form_port': form_port,
                'output': output_port,
            }
            return render(request, 'bootstrap/index.html', context_port)

    else:
        form_port = Consultas_Port()
        return render(request, 'bootstrap/index.html', {'form': form_port})

"""
"""            
            if InsideIbera:
                ingresoIp = InsideIbera
            if DMZIbera:
                ingresoIp = DMZIbera
            if InsideArcos:
                ingresoIp = InsideArcos
            if DMZServer:
                ingresoIp = DMZServer
            if DMZCISCO:
                ingresoIp = DMZCISCO
"""
"""
ingresoIp = "0"
InsideIbera = re.findall("^172.21.222", ingresoIp)
DMZIbera = re.findall("^192.168.21", ingresoIp)
InsideArcos = re.findall("^172.19", ingresoIp)
DMZServer = re.findall("^192.168.20", ingresoIp)
DMZCISCO = re.findall("^192.168.30", ingresoIp)

"""

"""
# Defino las variables donde asigno los segmentos de red
def NoPasaPorFW(dato):
    if InsideIbera:
        print("El equipo no pasa por el FW")
    if InsideArcos:
        print("El equipo no pasa por el FW")

"""
"""
# segun el segmento de red elijo el FW a conectarme
def ElijeFW(ingresoIp):
    ip = "0"
    if DMZIbera:
        ip = equipoIbera
    if DMZServer:
        ip = equipoArcos
    if DMZCISCO:
        ip = equipoArcos
    return ip

"""
"""
a = int(input("Ingrese un numero. no puede ser ni 5 ni 6: "))

while a == 5 or a == 6:
    print("Escoja el numero correcto: ")
    a = int(input("Ingrese un numero. no puede ser ni 5 ni 6: "))

print(a)
"""
# acordarse de DESCOMENTARRRRRRRRRRRR
"""
# segun el segmento de red elijo el FW a conectarme
def ingreso(ingresoIp):
    ip = "0"
    if DMZIbera:
        ip = equipoIbera
    if DMZServer:
        ip = equipoArcos
    if DMZCISCO:
        ip = equipoArcos
    else:
        print("No es un rango de IP valido")
    return ip
"""


"""# funcion que se conecta al FW y ejecuta el comando packet-tracer
def deviceConnection(interface,equipo, udp_tcp, iporigen, ipdestino, portori, portdest):
    ftd = {
        "device_type": "autodetect",
        "host": equipo,
        "username": "admin",
        "password": "secundop150",
    }

    # Show command that we execute
    command = f"packet-tracer input {interface} {udp_tcp} {iporigen} {portori} {ipdestino} {portdest}"

    with ConnectHandler(**ftd) as net_connect:
        output = net_connect.send_command(command)

    # Automatically cleans-up the output so that only the show output is returned
    print()
    print(output)
    print()
    return output
"""
# tomo el dato que retorna de la funcion ingreso() para usarlo en el
# diccionario de devices en el script de netmiko


"""
if match:
    print(Ibera)
else:
    print("No es IP valida")

 """

interfaceArcos = {
    "DMZ_Servers_20": "DMZ_Servers_20",
    "DMZ_CISCO": "DMZ_CISCO",
}

interfaceIbera = {
    "DMZ_21": "DMZ_21",
    "DMZ_Camaras": "DMZ_Camaras",
}

# print(interfaceArcos)

"""
#scrip de netmiko que realiza la conexion al FTD asignado por la funicion ingreso()
#ejecuta el comando "packet-tracer" que determina si un puerto esta o no permitido en el FW
cisco1 = {
    "device_type": "autodetect",
    "host": equipo,
    "username": "admin",
    "password": getpass(),
}

# Show command that we execute
command = "packet-tracer input DMZ_CISCO tcp 192.168.30.20 https 8.8.8.8 https"
with ConnectHandler(**cisco1) as net_connect:
    output = net_connect.send_command(command)

# Automatically cleans-up the output so that only the show output is returned
print()
print(output)
print()
"""
